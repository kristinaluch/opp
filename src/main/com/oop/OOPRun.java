package main.com.oop;

import main.com.oop.models.*;
import main.com.oop.services.DevicesServices;

public class OOPRun {
    public static void main(String[] args) {
        //������� ��������� ����������� ������ ProcessorArm, ProcessorX86, Memory

        ProcessorArm processorArm_1 = new ProcessorArm("3.2", "6", "4");
        ProcessorX86 processorX86_2 = new ProcessorX86("2.7", "7", "4");
        ProcessorArm processorArm_3 = new ProcessorArm("1.3", "8", "2");
        ProcessorX86 processorX86_4 = new ProcessorX86("2", "9", "4");
        ProcessorX86 processorX86_5 = new ProcessorX86("2", "9", "2");
        ProcessorArm processorArm_6 = new ProcessorArm("3.2", "6", "4");
        ProcessorX86 processorX86_7 = new ProcessorX86("2.7", "7", "4");
        ProcessorArm processorArm_8 = new ProcessorArm("1.3", "8", "2");
        ProcessorX86 processorX86_9 = new ProcessorX86("2", "9", "4");
        ProcessorArm processorArm_10 = new ProcessorArm("2", "9", "2");

        Processor[] processors = {processorArm_1, processorX86_2, processorArm_3, processorX86_4,
                processorX86_5, processorArm_6, processorX86_7, processorArm_8, processorX86_9, processorArm_10};


        Memory[] memories = new Memory[10];
        for (int i = 0; i < 10; i++) {
            memories[i] = new Memory();
        }

        Device[] devices = new Device[10];

        for (int i = 0; i < 10; i++) {
                devices[i] = new Device(processors[i], memories[i]);
        }
        DevicesServices devicesServices = new DevicesServices(devices);

//        ������� ������ Device[] (�� ����� 10�� ���������) � ��������� ����������� �����������, ������. ������������� ������� �� �������� ���������:
//        ������� � �������� ������������ ����������;
//        ���������� �� ���������� ����������;
//        ����� ������ ������/������ ��������� ��������
//        ����� �������������� ������ ������/������ ��������� ��������
//
//        ������ ���� ������� � ���������� ������ ���� �������������� unit �������.



    }
}
