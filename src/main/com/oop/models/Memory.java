package main.com.oop.models;

public class Memory {
//    ������� ����� Memory, ������� �������� ���� String[] memoryCell.
//    ���� ���������������� � ������������. ��������� �������� � null. ����� �������� 4 ������
//    String readLast() � �������� �������� �� ���������� ����������� �������� ������� (�������� �������� �� null),
//    String removeLast() � �������� ���������� �������� (�������� �������� null),
//    boolean save() � �������� � ��������� ������, �������� ������� null, ������� true, ���� ��������� ����� ������ ��� � ������� false;
//    getMemiryInfo() � ���������� ������, ��������� �� ���� �����: ����� ����� ������ (���������� ��������� �����), ������� ����� ������ (� ���������).

    private String[] memoryCell;
    private static final String ERROR_NO_ELEMENTS = "Error. Memory don't have elements";
    private static final String DELETED = "Element deleted";



    public void setMemoryCell(String[] memoryCell) {
        this.memoryCell = memoryCell;
    }

    public String[] getMemoryCell() {
        return memoryCell;
    }




    public String readLast(){
        int indEnd = memoryCell.length - 1;
        for (int ind = indEnd; ind >0 ; ind--) {
            if (memoryCell[ind] != null){
                return memoryCell[ind];
            }
        }
        return ERROR_NO_ELEMENTS;
    }

    public String removeLast(){

        int indEnd = memoryCell.length - 1;
        for (int ind = indEnd; ind >=0 ; ind--) {
            if (memoryCell[ind] != null){
                memoryCell[ind] = null;
                return DELETED;
            }
        }
        return ERROR_NO_ELEMENTS;
    }

    public boolean save(String element){
        if (memoryCell.length == 0){
            return false;
        }
        for (int ind = 0; ind < memoryCell.length ; ind++) {
            if (memoryCell[ind] == null){
                memoryCell[ind] = element;
                return true;
            }
        }
        return false;
    }

    public MemoryInfo getMemoryInfo(){

        int countFreeCeil = 0;
        int countUsedCeil = 0;

        for (int ind = 0; ind < memoryCell.length; ind++) {
            if (memoryCell[ind] == null){
                countFreeCeil++;
            }
            countUsedCeil++;
        }
        int percentOfUsedMemory = (countUsedCeil*100)/memoryCell.length;

        MemoryInfo memoryInfo = new MemoryInfo(countFreeCeil, percentOfUsedMemory);

        return memoryInfo;
    }


    public class MemoryInfo extends Memory{

        private int countFreeCeil;
        private int percentOfUsedMemory;

//        public MemoryInfo() {
//        }

        public MemoryInfo(int countFreeCeil, int percentOfUsedMemory) {
            this.countFreeCeil = countFreeCeil;
            this.percentOfUsedMemory = percentOfUsedMemory;
        }

        public int getCountFreeCeil() {
            return countFreeCeil;
        }

        public int getPercentOfUsedMemory() {
            return percentOfUsedMemory;
        }

        @Override
        public String toString() {
            return "MemoryInfo{" +
                    "countFreeCeil=" + countFreeCeil +
                    ", percentOfUsedMemory=" + percentOfUsedMemory +
                    '}';
        }
    }

//    getMemiryInfo() � ���������� ������, ��������� �� ���� �����: ����� ����� ������ (���������� ��������� �����),
//    ������� ����� ������ (� ���������).

}
