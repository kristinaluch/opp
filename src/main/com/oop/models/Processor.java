package main.com.oop.models;

public abstract class Processor {

      private String ARCHITECTURE;

      public String getARCHITECTURE() {
            return ARCHITECTURE;
      }

      //    ������� ����������� ����� Processor � ������:
//    frequency,
//    cache,
//    bitCapacity.
//    ������ ������ Processor:
//    getDetails(), ������� ���������� ����������� �������������� ���������� (�������� ����� � ���� ������,
//    String dataProcess(String data) � ����������� �����;
//    String dataProcess(long data) � ����������� �����.
//    ������� 2 ������ ���������� Processor: ProcessorArm, ProcessorX86. ����������������
      protected String frequency;
      protected String cache;
      protected String bitCapacity;

      public Processor(String frequency, String cache, String bitCapacity) {
            this.frequency = frequency;
            this.cache = cache;
            this.bitCapacity = bitCapacity;
      }

      public String getFrequency() {
            return frequency;
      }

      public String getCache() {
            return cache;
      }

      public String getBitCapacity() {
            return bitCapacity;
      }

      public String getDetails(){
            String details = "frequency "+ frequency + " cache "+cache+" bitCapacity "+bitCapacity;
          return details;
      }

      abstract String dataProcess(String data);

      abstract String dataProcess(long data);

}
