package main.com.oop.models;

public class ProcessorX86 extends Processor{

    private static final String platform = "Data process in X86: ";
    public static final String ARCHITECTURE = "X86";

    public ProcessorX86(String frequency, String cache, String bitCapacity) {
        super(frequency, cache, bitCapacity);
    }


    @Override
    String dataProcess(String data) {
        data = data.toLowerCase();
        String dataProcess = platform+data;
        return dataProcess;
    }

    @Override
    String dataProcess(long data) {
        String dataProcess = platform+data;
        return dataProcess;
    }
}
