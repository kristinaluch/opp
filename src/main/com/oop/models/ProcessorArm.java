package main.com.oop.models;

import java.util.Locale;

public class ProcessorArm extends Processor{

    private static final String platform = "Data process in Arm: ";
    public final String ARCHITECTURE = "ARM";

    public ProcessorArm(String frequency, String cache, String bitCapacity) {
        super(frequency, cache, bitCapacity);
    }


    @Override
    public String dataProcess(String data) {
        if (data == null){
            return "No process";
        }
        if(data.length() == 0){
            return "No process";
        }
        data = data.toUpperCase();
        String dataProcess = platform+data;
        return dataProcess;
    }

    @Override
    public String dataProcess(long data) {
        if(data == 0){
            return "No process";
        }
        String dataProcess = platform+data;
        return dataProcess;
    }
}
