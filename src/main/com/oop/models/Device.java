package main.com.oop.models;

public class Device {

    private  Processor processor;
    private Memory memory;

    public Device(Processor processor, Memory memory) {
        this.processor = processor;
        this.memory = memory;
    }

    public Processor getProcessor() {
        return processor;
    }

    public Memory getMemory() {
        return memory;
    }

    public void save (String[] data){
        memory.setMemoryCell(data);
    }

    public String[] readAll(){
        for (int i = memory.getMemoryCell().length -1; i >= 0; i--) {
            memory.readLast();
            memory.removeLast();
        }
        String[] newArray = memory.getMemoryCell();
        return newArray;
    }

    public void dataProcessing(){
        String[] process = memory.getMemoryCell();
        for (int i = 0; i >= memory.getMemoryCell().length; i++) {
            if(process[i] != null) {
                System.out.println(processor.dataProcess(process[i]));
                System.out.print(" " + processor.dataProcess(process[i].length()));
            }
        }
    }

    public String getSystemInfo(){
        Memory memoryInfo = memory.getMemoryInfo();
        String processorDetails = processor.getDetails();
        String systemInfo = processorDetails +" "+ memoryInfo.toString();

        return systemInfo;
    }




//    ������� ����� Device, ������� �������� ��������� ������:
//    void save (String[] data) � ���������� � ������ ���� ��������� � �������
//    String[] readAll() � ������� ���� ��������� �� ������, ����� �������� ������
//    void dataProcessing() � �������������� ���� ������, ���������� � ������
//    String getSystemInfo() � ��������� ������ � ����������� � ������� (���������� � ����������, ������)

}
