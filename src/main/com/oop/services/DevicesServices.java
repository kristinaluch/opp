package main.com.oop.services;

import main.com.oop.models.Device;
import main.com.oop.models.Memory;
import main.com.oop.models.Processor;

public class DevicesServices {
    //        ������� ������ Device[] (�� ����� 10�� ���������) � ��������� ����������� �����������, ������.
    //        ������������� ������� �� �������� ���������:
//        ������� � �������� ������������ ����������;
//        ���������� �� ���������� ����������;
//        ����� ������ ������/������ ��������� ��������
//        ����� �������������� ������ ������/������ ��������� ��������
//
//        ������ ���� ������� � ���������� ������ ���� �������������� unit �������.
    private Device[] devices;

    public DevicesServices(Device[] devices) {
        this.devices = devices;
    }

    public Device[] selectArchitecture(String searchArchitecture) {
        if (devices.length == 0){
            System.out.println("Data empty");
            return null;
        }
        Processor processor;
        String architecture;
        int length = 0;
        // ����� �� ������� ������ ������� � ��������� ������������
        for (int i = 0; i < devices.length; i++) {
            processor = devices[i].getProcessor();
            architecture = processor.getARCHITECTURE();
            if (architecture.equals(searchArchitecture)) {
                length++;
            }
        }
        if(length == 0){
            System.out.println("Not found");
            return null;
        }

        //����� �� ��� ���������
        Device[] select = new Device[length];
        int indSelect = 0;
        for (int i = 0; i < devices.length; i++) {
            processor = devices[i].getProcessor();
            architecture = processor.getARCHITECTURE();
            if (architecture.equals(searchArchitecture)) {
                select[indSelect] = devices[i];
                indSelect++;
            }
        }
        return select;
    }

    public Device[] selectOfParamProcessors(String paramName, String paramValue) {
        Device[] selectDev;
        if (paramName.equalsIgnoreCase("frequency")) {
            selectDev = selectOfFrequency(paramValue);
        } else if (paramName.equalsIgnoreCase("cache")) {
            selectDev = selectOfCache(paramValue);
        } else if (paramName.equalsIgnoreCase("bit capacity")) {
            selectDev = selectOfBitCapacity(paramValue);
        } else {
            System.out.println("No parameter name. Print frequency or cache or bit capacity");
            return null;
        }
        return selectDev;
    }

    private Device[] selectOfFrequency(String paramValue) {
        if (devices.length == 0){
            System.out.println("Data empty");
            return null;
        }
        Device[] select;
        boolean digit = paramValue.matches("(\\d*\\.\\d*)||\\d*");
        double value;
        double frequency;
        Processor processor;
        int length = 0;
        if (digit) {
            value = Double.parseDouble(paramValue);
            // ����� �� ������� ������ �������
            for (int i = 0; i < devices.length; i++) {
                processor = devices[i].getProcessor();
                frequency = Double.parseDouble(processor.getFrequency());
                if (frequency == value) {
                    length++;
                }
            }

            if(length == 0){
                System.out.println("Not found");
                return null;
            }
            //����� �� ��� ���������. ���� � �����.
            // SOLID �� ���������, �� ����� ��� ������������ ���������, � ������� ���.
            //�������� ��������� ����� �����������.
            int indSelect = 0;
            select = new Device[length];
            for (int i = 0; i < devices.length; i++) {
                processor = devices[i].getProcessor();
                frequency = Double.parseDouble(processor.getFrequency());
                if (frequency == value) {
                    select[indSelect] = devices[i];
                    indSelect++;
                }
            }
        } else {
            System.out.println("Error value");
            return null;
        }
        return select;
    }

    private Device[] selectOfCache(String paramValue) {
        if (devices.length == 0){
            System.out.println("Data empty");
            return null;
        }
        Device[] select;
        boolean digit = paramValue.matches("(\\d*\\.\\d*)||\\d*");
        double value;
        double cache;
        Processor processor;
        int length = 0;
        if (digit) {
            value = Double.parseDouble(paramValue);
            // ����� �� ������� ������ �������
            for (int i = 0; i < devices.length; i++) {
                processor = devices[i].getProcessor();
                cache = Double.parseDouble(processor.getCache());
                if (cache == value) {
                    length++;
                }
            }
            if(length == 0){
                System.out.println("Not found");
                return null;
            }
            //����� �� ��� ���������
            int indSelect = 0;
            select = new Device[length];
            for (int i = 0; i < devices.length; i++) {
                processor = devices[i].getProcessor();
                cache = Double.parseDouble(processor.getCache());
                if (cache == value) {
                    select[indSelect] = devices[i];
                    indSelect++;
                }
            }
        } else {
            System.out.println("Error value");
            return null;
        }
        return select;

    }

    private Device[] selectOfBitCapacity(String paramValue) {
        if (devices.length == 0){
            System.out.println("Data empty");
            return null;
        }
        Device[] select;
        boolean digit = paramValue.matches("(\\d*\\.\\d*)||\\d*");
        double value;
        double bitCapacity;
        Processor processor;
        int length = 0;
        if (digit) {
            value = Double.parseDouble(paramValue);
            // ������� ������ �������
            for (int i = 0; i < devices.length; i++) {
                processor = devices[i].getProcessor();
                bitCapacity = Double.parseDouble(processor.getBitCapacity());
                if (bitCapacity == value) {
                    length++;
                }
            }
            if(length == 0){
                System.out.println("Not found");
                return null;
            }
            //���������
            int indSelect = 0;
            select = new Device[length];
            for (int i = 0; i < devices.length; i++) {
                processor = devices[i].getProcessor();
                bitCapacity = Double.parseDouble(processor.getBitCapacity());
                if (bitCapacity == value) {
                    select[indSelect] = devices[i];
                    indSelect++;
                }
            }
        } else {
            System.out.println("Error value");
            return null;
        }
        return select;
    }

    public Device[] selectMemoryFree(String symbol, int paramValue){
        Device[] selectDev;
        int freeM = 0;
        if (symbol.equals(">")) {
            boolean param = freeM > paramValue;
            selectDev = selectMemoryFreeParam(paramValue, param, freeM);
        } else if (symbol.equals("<")) {
            boolean param = freeM < paramValue;
            selectDev = selectMemoryFreeParam(paramValue, param, freeM);
        } else {
            System.out.println("I'm error. Print > or <");
            return null;
        }
        return selectDev;
    }

    private Device[] selectMemoryFreeParam(int paramValue, boolean param, int freeM){
        if (devices.length == 0){
            System.out.println("Data empty");
            return null;
        }
        Device[] select;
        int length = 0;
        // ����� �� ������� ������ �������
        for (int i = 0; i < devices.length; i++) {
             freeM = devices[i].getMemory().getMemoryInfo().getCountFreeCeil();
            if (param) {
                length++;
            }
        }
        //����� �� ��� ���������. ���� � �����.
        // SOLID �� ���������, ����� ��� ������������ ���������, � ������� ���.
        //�������� ��������� ����� �����������.
        if(length == 0){
            System.out.println("Not found");
            return null;
        }
        int indSelect = 0;
        select = new Device[length];
        for (int i = 0; i < devices.length; i++) {
            freeM = devices[i].getMemory().getMemoryInfo().getCountFreeCeil();
            if (param) {
                select[indSelect] = devices[i];
                indSelect++;
            }
        }
        return select;
    }

    public Device[] selectMemoryUsed(String symbol, int paramValue){
        Device[] selectDev;
        int usedM = 0;
        if (symbol.equals(">")) {
            boolean param = usedM > paramValue;
            selectDev = selectMemoryUsedParam(paramValue, param, usedM);
        } else if (symbol.equals("<")) {
            boolean param = usedM < paramValue;
            selectDev = selectMemoryUsedParam(paramValue, param, usedM);
        } else {
            System.out.println("I'm error. Print > or <");
            return null;
        }
        return selectDev;
    }

    private Device[] selectMemoryUsedParam(int paramValue, boolean param, int usedM){
        if (devices.length == 0){
            System.out.println("Data empty");
            return null;
        }
        Device[] select;
        int length = 0;
        // ����� �� ������� ������ �������
        for (int i = 0; i < devices.length; i++) {
            usedM = devices[i].getMemory().getMemoryInfo().getPercentOfUsedMemory();
            if (param) {
                length++;
            }
        }
        //����� �� ��� ���������. ���� � �����.
        // SOLID �� ���������, ����� ��� ������������ ���������, � ������� ���.
        //�������� ��������� ����� �����������.
        if(length == 0){
            System.out.println("Not found");
            return null;
        }
        int indSelect = 0;
        select = new Device[length];
        for (int i = 0; i < devices.length; i++) {
            usedM = devices[i].getMemory().getMemoryInfo().getPercentOfUsedMemory();
            if (param) {
                select[indSelect] = devices[i];
                indSelect++;
            }
        }
        return select;
    }




}













