package test.com.oop.models;


import main.com.oop.models.Memory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class MemoryTest {

    private Memory cut = new Memory();
    private Memory.MemoryInfo memoryInfo;



    static Arguments[] readLastTestArgs(){
        return new Arguments[]{
                Arguments.arguments("City", new String[]{"Hollywood", "Undead", "City", null, null}),
                Arguments.arguments("Error. Memory don't have elements", new String[]{}),
                Arguments.arguments("stop", new  String[]{"1", "2", "3", "4", "5", "stop"})

        };
    }

    @ParameterizedTest
    @MethodSource("readLastTestArgs")
    void readLastTest(String expected, String[] memoryCeil){
        cut.setMemoryCell(memoryCeil);
        String actual = cut.readLast();
        Assertions.assertEquals(expected, actual);

    }

    static Arguments[] removeLastTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Element deleted", new String[]{"Hollywood", "Undead", "City", null, null}),
                Arguments.arguments("Error. Memory don't have elements", new String[]{}),
                Arguments.arguments("Element deleted", new  String[]{"1", "2", "3", "4", "5", "stop"})

        };
    }

    @ParameterizedTest
    @MethodSource("removeLastTestArgs")
    void removeLastTest(String expected,String[] memoryCeil ){
        cut.setMemoryCell(memoryCeil);
        String actual = cut.removeLast();
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] saveTestArgs(){
        return new Arguments[]{
                Arguments.arguments(true, new String[]{"Hollywood", "Undead", "City", null, null}, "Undead"),
                Arguments.arguments(false, new  String[]{"1", "2", "3", "4", "5", "stop"}, "go"),
                Arguments.arguments(false, new  String[0], "up")

        };
    }

    @ParameterizedTest
    @MethodSource("saveTestArgs")
    void saveTest(boolean expected, String[] memoryCeil , String element){
        cut.setMemoryCell(memoryCeil);
        boolean actual = cut.save(element);
        Assertions.assertEquals(expected,actual);
    }

}
