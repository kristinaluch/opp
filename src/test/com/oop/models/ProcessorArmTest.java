package test.com.oop.models;

import main.com.oop.models.ProcessorArm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

public class ProcessorArmTest {

    ProcessorArm cut = new ProcessorArm("2", "3", "4");

    static Arguments[] dataProcessTestArgs(){
        return new Arguments[]{
                Arguments.arguments("Data process in Arm: 1", "1"),
                Arguments.arguments("Data process in Arm: Y", "y"),
                Arguments.arguments("No process", ""),
                Arguments.arguments("No process", null),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcessTestArgs")
    void dataProcessTest(String expected, String data){
        String actual = cut.dataProcess(data);
        Assertions.assertEquals(expected,actual);
    }

    static Arguments[] dataProcess1TestArgs(){
        return new Arguments[]{
                Arguments.arguments("Data process in Arm: 1", 1),
                Arguments.arguments("Data process in Arm: 100", 100),
                Arguments.arguments("No process", 0),
        };
    }

    @ParameterizedTest
    @MethodSource("dataProcess1TestArgs")
    void dataProcess1Test(String expected, long data){
        String actual = cut.dataProcess(data);
        Assertions.assertEquals(expected,actual);
    }



}
